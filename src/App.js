import React from 'react';
import Container from 'react-bootstrap/Container'
import './App.scss';
import {BrowserRouter as Router, Link, Route} from "react-router-dom";
import ExpenditureList from "./expenditures/ExpenditureList";
import ExpenditureDetail from "./expenditures/ExpenditureDetail";
import Home from "./Home";

function App() {
  return (
    <Container>
      <Router>
        <div>
          <ul>
            <li>
              <Link to='/'>Home page</Link>
            </li>
            <li>
              <Link to='/expenditure-list'>All expenditures</Link>
            </li>
          </ul>
        </div>

        <Route exact path="/" component={Home} />
        <Route path="/expenditure-list" component={ExpenditureList} />
      </Router>
    </Container>
  );
}

export default App;
