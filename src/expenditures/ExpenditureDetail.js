import React from 'react';

class ExpenditureDetail extends React.Component {
  constructor(props) {
    super(props);
    this.id = props.id;
    this.price = props.price;
    this.qty = props.qty;
    this.date = props.date;
    this.item = props.item;
  }

  render() {
    return (
      <div>
        {this.item.name} x {this.qty}, {this.price} zł
      </div>
    )
  }
}

export default ExpenditureDetail;
