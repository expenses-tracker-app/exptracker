import React from 'react';
import axios from 'axios';
import ExpenditureDetail from "./ExpenditureDetail";
import ExpenditureAdd from "./ExpenditureAdd";

class ExpenditureList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      expenditures: []
    }
  }

  componentDidMount() {
    axios.get('http://127.0.0.1:8000/api/expenditure/').then(res => {
      console.log("axios GET");

      const expenditures = res.data.map((obj) =>
        <ExpenditureDetail
          id={obj.id}
          key={obj.id}
          price={obj.price}
          qty={obj.qty}
          date={obj.data}
          item={obj.item}
        />
      );

      this.setState({expenditures: expenditures})
    }, res => {
      console.log("error");
    })
  }

  render() {
    return (
      <div>
        <h1>All expenditures</h1>
        <div className="row">
          <div className="col">
            {this.state.expenditures}
          </div>
          <div className="col">
            <ExpenditureAdd />
          </div>
        </div>
      </div>
    )
  }
}

export default ExpenditureList;
